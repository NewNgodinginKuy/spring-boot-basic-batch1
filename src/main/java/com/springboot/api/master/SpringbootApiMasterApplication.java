package com.springboot.api.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApiMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApiMasterApplication.class, args);
	}

}
